<?php
class Pendaftaran extends CI_Controller {
	function __construct(){
		parent:: __construct();
		$this->load->model('Sop_Model');
		//$this->load->library('Bcrypt');
		//$this->load->library('Pdf');
		setlocale (LC_TIME, 'id_ID');
	}
	function index(){
		$page=$this->uri->segment(3);
		$data['page']="daftar_user";
		$data['open']='Pendaftaran/simpan_user';
		$data['kembali']='';
		$this->load->view('index2',$data);
	}
	function page(){
		$page=$this->uri->segment(3);
		$data['page']=$page;
		if($page=="daftar_user"){
			$data['open']='Pendaftaran/simpan_user';
			$data['kembali']='';
			//$data['tmp_pt']=$this->Sop_Model->qw("*","user","")->result();
		}elseif($page=="lihat_form"){
			$data['tmp_pt']=$this->Sop_Model->qw("*","master_form","ORDER BY id ASC")->result();
		}
		$this->load->view('index2',$data);
	}

	function getmhsw(){
		$nim=$this->uri->segment(3);
		$data = "https://sima.usm.ac.id/rest_api/rest_ltc/mhs/".$nim;
  		$hasil = file_get_contents($data);
  		$hasil = trim(preg_replace('/\s\s+/', '', $hasil));
  		$hasil=str_replace(array("\r\n","\r","\n"),"",$hasil);
  		$hasil= str_replace("Array(","{",$hasil);
  		$hasil= str_replace("[",',"',$hasil);
  		$hasil= str_replace("] => ",'" : "',$hasil);
  		$hasil= str_replace(',"','","',$hasil);
  		$hasil= str_replace(')','"}',$hasil);
  		$hasil= str_replace('{",','{',$hasil);
  		$hasil= str_replace(': "{',':{',$hasil);
  		$hasil= str_replace('}"','}',$hasil);
  		echo $hasil;	
	}

	function lokasi_file($jenis_file,$id_ijin)
	{
		$getfile=$this->Sop_Model->qw("*","file_peserta","WHERE id_ijin='$id_ijin' AND jenis_file='$jenis_file'")->row_array();
		return $getfile;
	}


	function simpan_user(){
		$nama = $this->input->post('nama');
		$nik = $this->input->post('nik');
		$alamat = $this->input->post('alamat');
		$telp = $this->input->post('telp');
		$email = $this->input->post('email');
		$profesi = $this->input->post('profesi');
		$nama_kelp = $this->input->post('nama_kelp');
		$paket = $this->input->post('paket');
		$tanggal = $this->input->post('tanggal');
		
		//memasukkan table kelas
		$ary=array(
			'nik'	=>$nik,
			'nama'	=>$nama,
			'alamat' =>$alamat,
			'telp' =>$telp,
			'email' =>$email,
			'profesi' =>$profesi,
			'nama_kelp' =>$nama_kelp,
			'paket' =>$paket,
			'tanggal' =>$tanggal,
		);
		$this->Sop_Model->simpan_peserta('peserta',$ary);
		redirect('Pendaftaran/page/berhasil');
	}

    function logout(){
		$this->session->sess_destroy();
		redirect('LoginAdmin_Controller');
	}
}
