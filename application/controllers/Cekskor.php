<?php
class Cekskor extends CI_Controller {
	function __construct(){
		parent:: __construct();
		$this->load->model('Sop_Model');
		//$this->load->library('Bcrypt');
		//$this->load->library('Pdf');
		setlocale (LC_TIME, 'id_ID');
	}
	function page(){
		$page=$this->uri->segment(3);
		$data['page']=$page;
		if($page=="cek"){
			$data['open']='Cekskor/cek';
			$data['kembali']='';
			//$data['tmp_pt']=$this->Sop_Model->qw("*","user","")->result();
		}elseif($page=="lihat_form"){
			$data['tmp_pt']=$this->Sop_Model->qw("*","master_form","ORDER BY id ASC")->result();
		}
		$this->load->view('index3',$data);
	}


	function cek()
	{
		$noreg = $this->input->post('noreg');
		$getreg=$this->Sop_Model->qw("*","t_peserta_ujian","WHERE no_reg='$noreg'")->num_rows();
		if($getreg==0)
		{
			redirect('Cekskor/page/cek/1');
		}else{
			redirect('Cekskor/page/hasil/'.$noreg);
		}
	}


	function simpan_user(){
		$nama = $this->input->post('nama');
		$namamhsw = $this->input->post('namamhsw');
		
		$alamat = $this->input->post('alamat');
		$nim = $this->input->post('nim');
		$kodekhusus = $this->input->post('kodekhusus');
		$prodi = $this->input->post('prodi');
		$fak = $this->input->post('fak');
		$telp = $this->input->post('telp');
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$password_en = password_hash($password, PASSWORD_DEFAULT);
		$tahun = date("Y") . "" . 1;
		if(empty($nama) || $nama=='' || !isset($nama)) $nm=$namamhsw; else $nm=$nama;
		//echo "namanya ".$nm;
		//exit();
		$hitung=$this->Sop_Model->qw("*","tblsiswa","WHERE nis='$email'"
        )->num_rows();

        if($hitung>0)
        {
        	$error = 1;
        	redirect('Pendaftaran/page/daftar_user/'.$error);
        }

        if(strlen($password)<8)
        {
        	$error = 2;
        	redirect('Pendaftaran/page/daftar_user/'.$error);
        }
		
		//memasukkan table kelas
		$ary=array(
			'Kelas'	=>$email,
			'nip_wali'	=>'',
			'tahun' =>$tahun,
			'Active'		=>'Y'
		);
		$this->Sop_Model->simpan_tambah_kelas('tblkelas',$ary);
		$insert_id = $this->db->insert_id();

		$ary2=array(
			'nis'		=>$email,
			'nama'		=>$nm,
			'IDKelas' 	=>$insert_id,
			'email'		=>$email,
			'alamat'	=>$alamat,
			'status'	=>'Y',
			'agama'		=>'',
			'nim'		=>$nim,
			'kodekhusus'		=>$kodekhusus,
			'prodi'		=>$prodi,
			'fak'		=>$fak
		);
		$this->Sop_Model->simpan_tambah_siswa('tblsiswa',$ary2);
		$insert_id2 = $this->db->insert_id();

		$ary3=array(
			'id'			=>$insert_id2,
			'username'		=>$email,
			'password' 		=>$password_en,
			'status'		=>1,
			'jurusan'		=>1,
			'nis'			=>$email,
			'nama'			=>$nm,
			'aktif'			=>0
		);
		$this->Sop_Model->simpan_tambah_peserta('t_peserta',$ary3);
		redirect('Pendaftaran/page/daftar_user/3');
	}

    function logout(){
		$this->session->sess_destroy();
		redirect('LoginAdmin_Controller');
	}
}
