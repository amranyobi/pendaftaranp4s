<?php
	class Sop_Model extends CI_Model{
		function qw($cel,$table,$prop){
			return $this->db->query("SELECT $cel FROM $table $prop");
		}
		function simpan_user($table,$value){
			return $this->db->insert($table,$value);
		}
		function simpan_prosedur($table,$value){
			return $this->db->insert($table,$value);
		}
		function edit_prosedur($table,$value,$id){
			return $this->db->update($table,$value,$id);
		}
		function edit_fbk($table,$where,$value){
			$this->db->where('no_sop',$where);
			return $this->db->update($table,$value);
		}
		function simpan_fbk($table,$value){
			return $this->db->insert($table,$value);
		}
		function hapus_prosedur($table,$id)
		{
			return $this->db->delete($table,$id);
		}
		function edit_user($table,$where,$value){
			$this->db->where('id_user',$where);
			return $this->db->update($table,$value);
		}
		function hapus_user($table,$where){
			$this->db->where('id_user',$where);
			return $this->db->delete($table);
		}
		function simpan_pelaksana($table,$value){
			return $this->db->insert($table,$value);
		}
		function edit_pelaksana($table,$where,$value){
			$this->db->where('id_pelaksana',$where);
			return $this->db->update($table,$value);
		}
		function simpan_identitas_aplikasi($table,$value){
			return $this->db->insert($table,$value);
		}
		function edit_identitas_aplikasi($table,$where,$value){
			$this->db->where('id_identitas',$where);
			return $this->db->update($table,$value);
		}
		function hapus_pelaksana($table,$where){
			$this->db->where('id_pelaksana',$where);
			return $this->db->delete($table);
		}
		function simpan_identitas_sop($table,$value){
			return $this->db->insert($table,$value);
		}
		function edit_identitas_sop($table,$where,$value){
			$this->db->where('no_sop',$where);
			return $this->db->update($table,$value);
		}
		function hapus_identitas_sop($table,$where){
			$this->db->where('no_sop',$where);
			return $this->db->delete($table);
		}
		function hapus_identitas_aplikasi($table,$where){
			$this->db->where('id_identitas',$where);
			return $this->db->delete($table);
		}
	}
?>
