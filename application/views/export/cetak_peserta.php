<?php
// Fungsi header dengan mengirimkan raw data excel
header("Content-type: application/vnd-ms-excel");

// Mendefinisikan nama file ekspor "hasil-export.xls"
header("Content-Disposition: attachment; filename=data_peserta_pelatihan.xls");
$tanggal = $this->uri->segment(4);

//cek pernah ujian
if($tanggal=='')
  $data_peserta = $this->Sop_Model->qw("*","peserta","ORDER BY tanggal DESC, nama ASC")->result();
else
  $data_peserta = $this->Sop_Model->qw("*","peserta","WHERE tanggal='$tanggal' ORDER BY tanggal DESC, nama ASC")->result();
?>
   <section class="content">

      <div class="row">

        <div class="col-xs-12">

          <div class="box box-primary">
            <div class="box-body">
              <h1>DATA PESERTA PELATIHAN</h1>
              <table border="1" class="table table-bordered table-striped display">

                <thead>

                <tr>

                  <th>No</th>
                  <th>NIK</th>
                  <th>Nama</th>
                  <th>Alamat</th>
                  <th>Telp</th>
                  <th>Email</th>
                  <th>Profesi</th>
                  <th>Nama Kelompok</th>
                  <th>Paket</th>
                  <th>Tanggal</th>

                </tr>

                </thead>

                <tbody>

                <?php

                  $no=0;

                  foreach($data_peserta as $tampil){

                  $no++;

                ?>

                <tr>

                  <td><?php echo $no;?></td>
                  <td><?php echo $tampil->nik?></td>
                  <td><?php echo $tampil->nama?></td>
                  <td><?php echo $tampil->alamat?></td>
                  <td>'<?php echo $tampil->telp?></td>
                  <td><?php echo $tampil->email?></td>
                  <td><?php
                  if($tampil->profesi=='1')
                    echo "Petani";
                  else
                    echo "Peternak";?></td>
                  <td><?php echo $tampil->nama_kelp?></td>
                  <td><?php
                  $paket = $this->Sop_Model->qw("*","master_paket","WHERE id_paket='$tampil->paket'")->row_array();
                  echo "Paket ";
                  echo $paket['paket'];
                  echo " : ";
                  echo $paket['mata_diklat'];
                  echo " - ";
                  echo $paket['kurikulum'];
                  echo " (";
                  echo number_format($paket['biaya']);
                  echo ")";?></td>
                  <td><?php echo date("d-m-Y",strtotime($tampil->tanggal))?></td>

                </tr>

                <?php } ?>

                </tbody>

              </table>

            </div>
            

            

            <!-- /.box-body -->

          </div>

          <!-- /.box -->

        </div>

        <!-- /.col -->

      </div>

      <!-- /.row -->

    </section>