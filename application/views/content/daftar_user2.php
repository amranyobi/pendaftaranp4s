<style>
  #showNama {
  display: none; // you're using diaplay!
}
</style>
<?php
$error=$this->uri->segment(4);
//header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: GET, OPTIONS");
header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding");
?>
<section class="content-header">
      <h1>
        Pendaftaran User
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Form</a></li>
        <li class="active">Pendaftaran User</li>
      </ol>
    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">

            <?php
            if($error=='1')
            {
              ?>
              <br>
              <div class="callout callout-danger" style="margin-left: 10px; margin-right: 10px">
              <h4>Email sudah pernah didaftarkan</h4>
              <p>Gunakan email yang lain untuk melakukan pendaftaran user</p>
              </div>
              <?php
            }elseif($error=='2')
            {
              ?>
              <br>
              <div class="callout callout-danger" style="margin-left: 10px; margin-right: 10px">
              <h4>Pasword kurang dari 8 (delapan) karakter</h4>
              <p>Masukkan password minimal 8 (delapan) kombinasi karakter</p>
              </div>
              <?php
            }elseif($error=='3')
            {
              ?>
              <br>
              <div class="callout callout-success" style="margin-left: 10px; margin-right: 10px">
              <h4>Pendaftaran User Berhasil!</h4>
              <p>Pendaftaran User telah berhasil, Silahkan login menggunakan username (Email) dan password yang telah didaftarkan untuk memilih jadwal ujian yang ingin diikuti</p>
              </div>
              <?php
            }
            ?>

            <form role="form" class="form-horizontal" action="<?php echo site_url($open);?>" method="POST" enctype="multipart/form-data">
              <div class="box-body">
                <div class="row">
                  <div class="col-md-6">
                   <!--<div class="form-group">
                      <label class="col-sm-3">Tipe Pendaftar</label>
                      <div class="col-sm-4">
                      <select onclick="status_change();" class="custom-select rounded-0" name="pendaftar" id="pendaftar">
                      <option value="mahasiswa">Mahasiswa USM</option>
                      <option value="umum" >Umum</option>
                      </div>
                    </div> -->
                    <div class="form-group">
                  <label  class="col-sm-3" for="exampleSelectBorder">Tipe Pendaftar</label>
                  <div class="col-sm-9">
                  <select class="custom-select form-control-border" onclick="status_change();" name="pendaftar" id="pendaftar">
                  <option value="mahasiswa">Mahasiswa USM</option>
                      <option value="umum" >Umum</option>
                  </select>
          </div>
                </div>
               
                <div id="kategdaftar" style="display: inline;">
                    <div class="form-group">
                      <label class="col-sm-3">NIM</label>
                      <div class="col-sm-9">
                        <input type="text" class="form-control" name="nim" id="nim" autocomplete="off" onChange="ceknim_change();">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-3">Nama</label>
                      <div class="col-sm-9">
                      <span id="nama_mhsw"></span>  
                      <input type="hidden" class="form-control" name="namamhsw" id="namamhsw" >
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-3">Kode Khusus</label>
                      <div class="col-sm-9">
                      <span id="kode_khusus"></span>
                        <input type="hidden" class="form-control" name="kodekhusus" id="kodekhusus">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-3">Program Studi</label>
                      <div class="col-sm-9">
                      <span id="prodi2"></span>
                        <input type="hidden" class="form-control" name="prodi" id="prodi">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-3">Fakultas</label>
                      <div class="col-sm-9">
                      <span id="fak2"></span>
                        <input type="hidden" class="form-control" name="fak" id="fak">
                      </div>
                    </div>
                    </div>
                    <div id="showNama">
                    <div class="form-group">
                      <label class="col-sm-3">NIK</label>
                      <div class="col-sm-6">
                        <input type="text" class="form-control" name="nik" autocomplete="off">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-3">Nama</label>
                      <div class="col-sm-9">
                        <input type="text" class="form-control" name="nama" autocomplete="off">
                      </div>
                    </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-3">Alamat</label>
                      <div class="col-sm-9">
                        <textarea class="form-control" name="alamat"></textarea>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-3">No. Telp / HP (Whatsapp aktif)</label>
                      <div class="col-sm-4">
                        <input type="text" class="form-control" name="telp" autocomplete="off" value="<?php
                        if(isset($data_sudah['telp']))
                        echo $data_sudah['telp']?>">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-3"></label>
                      <div class="col-sm-9">
                        <b><br>Masukkan username (menggunakan email) dan password</b>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-3">Email</label>
                      <div class="col-sm-6">
                        <input type="email" class="form-control" name="email" autocomplete="off">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-3">Password</label>
                      <div class="col-sm-6">
                        <input type="password" id="txtPassword" class="form-control" name="password" autocomplete="off">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-3">Password (konfirmasi)</label>
                      <div class="col-sm-6">
                        <input type="password" id="txtConfirmPassword" class="form-control" name="password_konfirmasi" autocomplete="off">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              </div>
              </div>

              <div class="box-footer">
                <button type="submit" id="btnSubmit" name="" class="btn btn-primary"><i class="fa fa-save"></i> Daftar</button>
                  <a href="<?php echo site_url($kembali);?>" class="btn btn-danger">Kembali</a>
              </div>
            </form>
          </div>
          <!-- /.box -->

          <!-- Form Element sizes -->
         
          <!-- /.box -->

          
          <!-- /.box -->

          <!-- Input addon -->
          
          <!-- /.box -->
        </div>
      </div>
    </section>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $("#btnSubmit").click(function () {
                var password = $("#txtPassword").val();
                var confirmPassword = $("#txtConfirmPassword").val();
                if (password != confirmPassword) {
                    alert("Isian Password dan Konfirmasi Password Tidak Sama.");
                    return false;
                }
                return true;
            });
        });
        $('#showNama').hide();
        $("#showNama").css('visibility', 'visible');
        var x = document.getElementById("pendaftar").value;
            if (x=='mahasiswa') {
                $('#kategdaftar').show();
                $('#showNama').hide();
            } else {
                $('#kategdaftar').hide();
                $('#showNama').show();
            }
        /*$(document).ready(function() {
            $("input#nim").on("change", function() {
                var id = this.value;
                $.ajax({
                    url: "<?php //echo base_url()."Pendaftaran/getmhsw/";?>"+id,
                    type: "GET", 
                    dataType: 'json',
                    success: function (response) {
                      $("#namamhsw").val(response.result.nama_mahasiswa);
                      $("#kodekhusus").val(response.result.kode_khusus);
                      $("#prodi").val(response.result.programstudi);
                      $("#fak").val(response.result.fakultas);
                      console.log(response);
                    },
                    error: function () {
                      console.log("error");
                    }
                });
                alert('tes');
            });
        });*/

        function ceknim_change() {
            
            var id = document.getElementById("nim").value;
                  $.ajax({
                      url: "http://app.inhealth.co.id/pelkesws2/api/EligibilitasPeserta",
                      type: "POST",
                      data: {
                      "token": "inh_22009d5f2a1001a9812227f58c5faf9d",
                      "kodeprovider": "1101G042",
                      "tglpelayanan": "2017-11-03",
                      "nokainhealth": "1001460689482",
                      "jenispelayanan": "3",
                      "poli": "GIG",
                      },
                      dataType: 'json',
                      cache: false,
                      success: function (response) {
                        // $("#namamhsw").val(response.result.nama_mahasiswa);
                        // $("#nama_mhsw").text(response.result.nama_mahasiswa);
                        // $("#kode_khusus").text(response.result.kode_khusus);
                        // $("#prodi2").text(response.result.programstudi);
                        // $("#fak2").text(response.result.fakultas);
                        // $("#kodekhusus").val(response.result.kode_khusus);
                        // $("#prodi").val(response.result.programstudi);
                        // $("#fak").val(response.result.fakultas);
                        // $("#namamhsw").attr("type","hidden");
                        //   $("#kodekhusus").attr("type","hidden");
                        //   $("#prodi").attr("type","hidden");
                        //   $("#fak").attr("type","hidden");
                        console.log(response);
                      },
                      error: function () {
                        console.log("error");
                        if (confirm("Maaf, anda salah memasukkan NIM Anda atau NIM belum terdaftar dalam server. Apakah anda ingin input data secara manual?")) {
                            // var hidden_fields = $(this).find( 'input:hidden' );
                            // // Filter those which have a specific type
                            // hidden_fields.attr('text');
                          $("#namamhsw").attr("type","text");
                          $("#kodekhusus").attr("type","text");
                          $("#prodi").attr("type","text");
                          $("#fak").attr("type","text");
                        }
                        else {
                          $("#namamhsw").attr("type","hidden");
                          $("#kodekhusus").attr("type","hidden");
                          $("#prodi").attr("type","hidden");
                          $("#fak").attr("type","hidden");
                        }
  
                        $("#namamhsw").val('');
                        $("#kodekhusus").val('');
                        $("#prodi").val('');
                        $("#fak").val('');
                        $("#nama_mhsw").text('');
                        $("#kode_khusus").text('');
                        $("#prodi2").text('');
                        $("#fak2").text('');
                      }
                  });
                  
             
          }

          // function ceknim_change() {
              
          //     var id = document.getElementById("nim").value;
          //           $.ajax({
          //               url: "<?php echo base_url()."Pendaftaran/getmhsw/";?>"+id,
          //               type: "GET", 
          //               dataType: 'json',
          //               success: function (response) {
          //                 $("#namamhsw").val(response.result.nama_mahasiswa);
          //                 $("#nama_mhsw").text(response.result.nama_mahasiswa);
          //                 $("#kode_khusus").text(response.result.kode_khusus);
          //                 $("#prodi2").text(response.result.programstudi);
          //                 $("#fak2").text(response.result.fakultas);
          //                 $("#kodekhusus").val(response.result.kode_khusus);
          //                 $("#prodi").val(response.result.programstudi);
          //                 $("#fak").val(response.result.fakultas);
          //                 $("#namamhsw").attr("type","hidden");
          //                   $("#kodekhusus").attr("type","hidden");
          //                   $("#prodi").attr("type","hidden");
          //                   $("#fak").attr("type","hidden");
          //                 console.log(response);
          //               },
          //               error: function () {
          //                 console.log("error");
          //                 if (confirm("Maaf, anda salah memasukkan NIM Anda atau NIM belum terdaftar dalam server. Apakah anda ingin input data secara manual?")) {
          //                     // var hidden_fields = $(this).find( 'input:hidden' );
          //                     // // Filter those which have a specific type
          //                     // hidden_fields.attr('text');
          //                   $("#namamhsw").attr("type","text");
          //                   $("#kodekhusus").attr("type","text");
          //                   $("#prodi").attr("type","text");
          //                   $("#fak").attr("type","text");
          //                 }
          //                 else {
          //                   $("#namamhsw").attr("type","hidden");
          //                   $("#kodekhusus").attr("type","hidden");
          //                   $("#prodi").attr("type","hidden");
          //                   $("#fak").attr("type","hidden");
          //                 }
          
          //                 $("#namamhsw").val('');
          //                 $("#kodekhusus").val('');
          //                 $("#prodi").val('');
          //                 $("#fak").val('');
          //                 $("#nama_mhsw").text('');
          //                 $("#kode_khusus").text('');
          //                 $("#prodi2").text('');
          //                 $("#fak2").text('');
          //               }
          //           });
                    
               
          //   }
        
        function status_change() {
            var x = document.getElementById("pendaftar").value;
            if (x=='mahasiswa') {
                $('#kategdaftar').show();
                $('#showNama').hide();
            } else {
                $('#kategdaftar').hide();
                $('#showNama').show();
            }
          }
    </script>