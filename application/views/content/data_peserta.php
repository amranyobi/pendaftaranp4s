<?php
$tanggal = $this->uri->segment(4);
$berhasil=$this->uri->segment(5);
$email = $this->session->userdata('email');
$date = date("Y-m-d");

//cek pernah ujian
if($tanggal=='')
  $data_peserta = $this->Sop_Model->qw("*","peserta","ORDER BY tanggal DESC, nama ASC")->result();
else
  $data_peserta = $this->Sop_Model->qw("*","peserta","WHERE tanggal='$tanggal' ORDER BY tanggal DESC, nama ASC")->result();
if($berhasil!='')
{
    $kata = "Tambah";
    $call = "success";
}
$filter_tanggal = 'Sop_Controller/filter_tanggal/';
$kembali = 'Sop_Controller/page/data_peserta';
?>
<section class="content-header">

      <h1>
        Data Peserta Pelatihan
      </h1>

      <ol class="breadcrumb">

        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

        <li><a href="#">Data</a></li>

        <li class="active">Peserta Pelatihan</li>

      </ol>

</section>



   <section class="content">

      <div class="row">

        <div class="col-xs-12">

          <div class="box box-primary">
            <div class="box-body">
              <div class="row" style="margin-bottom: 10px">
                <div class="col-md-10">
                  <form role="form" class="form-horizontal" action="<?php echo site_url($filter_tanggal); ?>" method="POST" enctype="multipart/form-data">
                    <label class="col-sm-5"><div align="right">Pilih Per Tanggal</div></label>
                    <div class="col-sm-2"> 
                      <input type="text" class="form-control" name="filter_tanggal" id="datepickera" value="<?php 
                      if($tanggal!='')
                      {
                        echo $tanggal;  
                      }
                      ?>" autocomplete="off">
                    </div>
                    <div class="col-sm-4">
                      <button type="submit" name="" class="btn btn-primary">Filter</button>
                      <a href="<?php echo site_url($kembali);?>"  class="btn btn-success">Lihat Semua</a>
                    </div>
                  </form>
                </div>
              </div>

              <div class="box-header" style="margin-top: 20px;">
                <div align="right" style="margin-right: 20px">
                  <a href="<?php echo site_url("Sop_Controller/cetak_peserta/$tanggal"); ?>" class="btn btn-sm btn-success" target="_blank"><i class="fa fa-file-excel-o"></i> Export</a>
                </div>
              </div>

              <table id="example1" class="table table-bordered table-striped display">

                <thead>

                <tr>

                  <th>No</th>
                  <th>Nama (NIK)</th>
                  <th>Alamat</th>
                  <th>Telp</th>
                  <th>Email</th>
                  <th>Profesi<br>(Nama Kelompok)</th>
                  <th>Paket</th>
                  <th>Tanggal</th>

                </tr>

                </thead>

                <tbody>

                <?php

                  $no=0;

                  foreach($data_peserta as $tampil){

                  $no++;

                ?>

                <tr>

                  <td><?php echo $no;?></td>
                  <td><?php echo $tampil->nama?><br>(<?php echo $tampil->nik?>)</td>
                  <td><?php echo $tampil->alamat?></td>
                  <td><?php echo $tampil->telp?></td>
                  <td><?php echo $tampil->email?></td>
                  <td><?php
                  if($tampil->profesi=='1')
                    echo "Petani";
                  else
                    echo "Peternak";?></td>
                  <td><?php
                  $paket = $this->Sop_Model->qw("*","master_paket","WHERE id_paket='$tampil->paket'")->row_array();
                  echo "Paket ";
                  echo $paket['paket'];
                  echo " : ";
                  echo $paket['mata_diklat'];
                  echo " - ";
                  echo $paket['kurikulum'];
                  echo " (";
                  echo number_format($paket['biaya']);
                  echo ")";?></td>
                  <td><?php echo date("d-m-Y",strtotime($tampil->tanggal))?></td>

                </tr>

                <?php } ?>

                </tbody>

              </table>

            </div>
            

            

            <!-- /.box-body -->

          </div>

          <!-- /.box -->

        </div>

        <!-- /.col -->

      </div>

      <!-- /.row -->

    </section>