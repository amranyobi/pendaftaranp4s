<style>
  #showNama {
  display: none; // you're using diaplay!
}
</style>
<?php
$error=$this->uri->segment(4);
//header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');
?>
<section class="content-header">
      <h1>
        Cek Hasil Skor
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Form</a></li>
        <li class="active">Cek Hasil Skor</li>
      </ol>
    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">

            <?php
            if($error=='1')
            {
              ?>
              <br>
              <div class="callout callout-danger style="margin-left: 10px; margin-right: 10px">
              <h4>Nomor Registrasi Tidak Ditemukan</h4>
              <p>Nomor Registrasi yang Anda masukkan tidak ditemukan, silahkan masukkan nomor lain. Nomor Registrasi dapat dilihat di Sertifikat</p>
              </div>
              <?php
            }
            ?>

            <form role="form" class="form-horizontal" action="<?php echo site_url($open);?>" method="POST" enctype="multipart/form-data">
              <div class="box-body">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="col-sm-3">Masukkan No. Registrasi</label>
                      <div class="col-sm-6">
                        <input type="text" class="form-control" name="noreg" autocomplete="off" required placeholder="Contoh : TFLXXXXXXX">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              </div>
              </div>

              <div class="box-footer">
                <button type="submit" id="btnSubmit" name="" class="btn btn-primary"><i class="fa fa-check"></i> Cek</button>
                  <a href="<?php echo site_url($kembali);?>" class="btn btn-danger">Batal</a>
              </div>
            </form>
          </div>
          <!-- /.box -->

          <!-- Form Element sizes -->
         
          <!-- /.box -->

          
          <!-- /.box -->

          <!-- Input addon -->
          
          <!-- /.box -->
        </div>
      </div>
    </section>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>