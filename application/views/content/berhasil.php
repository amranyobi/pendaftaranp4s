<section class="content-header">
      <h1>
        Pendaftaran Berhasil
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Form</a></li>
        <li class="active">Pendaftaran Berhasil</li>
      </ol>
    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <form role="form" class="form-horizontal" action="" method="POST" enctype="multipart/form-data">
              <div class="box-body">
                <div class="row">
                  <div class="col-md-7">
                    Pendaftaran Pelatihan telah berhasil dilakukan<br>
                    Silahkan dapat melakukan pembayaran pelatihan ke Nomor Rekening :<br>
                    <br>
                    <b>
                    Bank Jateng<br>
                    a.n HERRY RUSTANTO<br>
                    3-025-03962-1
                    <br><br></b>
                    Selanjutnya dapat melakukan konfirmasi pembayaran melalui Whatsapp melalui nomor Berikut :<br>
                    <b>083861718444<br>
                    a.n Herry Rustanto</b>
                  </div>
                </div>
              </div>
              </div>
              </div>

              <div class="box-footer">
                <a href="<?php echo site_url('pendaftaran');?>" class="btn btn-danger">Daftar Lagi</a>
              </div>
            </form>
          </div>
          <!-- /.box -->

          <!-- Form Element sizes -->
         
          <!-- /.box -->

          
          <!-- /.box -->

          <!-- Input addon -->
          
          <!-- /.box -->
        </div>
      </div>
    </section>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>